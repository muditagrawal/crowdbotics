from django.contrib import admin
from .models import Cat, Dog, Owner

@admin.register(Cat)
class CatAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_filter = ('name', )

@admin.register(Dog)
class DogAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_filter = ('name', )

admin.site.register(Owner)


    