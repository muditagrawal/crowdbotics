from rest_framework import serializers
from .models import Cat, Dog

class CatSerializers(serializers.ModelSerializer):
    class Meta:
        model = Cat
        fields = (
            'id', 'name','birthday',)

class DogSerializers(serializers.ModelSerializer):
    class Meta:
        model = Dog
        fields = (
            'id', 'name','birthday',)



