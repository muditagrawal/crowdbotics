from django.shortcuts import render

from django.http import HttpResponse


from .models import Dog, Cat, Owner
from django.views import View
from django.shortcuts import Http404
from django.http import JsonResponse
from .serializers import CatSerializers, DogSerializers
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator



import json
from django.core import serializers
# import simplejson as json


@method_decorator(csrf_exempt, name='dispatch')
class DogView(View):
    def get(self, request):
        try:
            dog = Dog.objects.all()
    
        except:
    
            raise Http404("Not Found")
        # serializer = DogSerializers(dog)
        return JsonResponse(serializers.serialize('json',dog), safe=False)  

    def post(self, request):
        data = request.body.decode('utf-8')
        json_data = json.loads(data)
        name = json_data.get('name', None)
        birthday = json_data.get('birthday', None)
        owner = json_data.get('owner', None)

        dog, created = Dog.objects.get_or_create(
            name=name, birthday=birthday, owner__name__iexact=owner
        )
        if created:
            return JsonResponse({'status': 'success'})
        else:
            return JsonResponse({'status':'unsuccessfull'})

    def Put(self, request):
        data = request.body.decode('utf-8')
        json_data = json.loads(data)
        id = json_data.get('id', None)
        name = json_data.get('name', None)
        birthday = json_data.get('birthday', None)
        owner_name = json_data.get('owner', None)

        owner_obj = Owner.objects.filter(owner__name=owner_name).first()

        dog_object = dog.objects.filter(id=id).first()
        dog_object.update(owner=owner_obj, name=name, birthday=birthday)

        return JsonResponse({'status': 'successfully updated'})
    def delete(self, request):
        data = request.body.decode('utf-8')
        json_data = json.loads(data)
        id = json_data.get('id', None)
        if id:
            Dog.objects.filter(id=id).first().delete()
            return JsonResponse({'status': 'successfully deleted'})
        return  JsonResponse({'status': 'unsuccessfull'})
        

@method_decorator(csrf_exempt, name='dispatch')
class CatView(View):
    def get(self, request):
        try:
            cat = Dog.objects.all()
        except:
            raise Http404("Not Found")
        
        # serializer = DogSerializers(dog)
        return JsonResponse(serializers.serialize('json', cat), safe=False)  

    def post(self, request):
        data = request.body.decode('utf-8')
        json_data = json.loads(data)
        name = json_data.get('name', None)
        birthday = json_data.get('birthday', None)
        owner = json_data.get('owner', None)

        cat, created = Cat.objects.get_or_create(
            name=name, birthday=birthday, owner__name__iexact=owner
        )
        if created:
            return JsonResponse({'status': 'success'})
        else:
            return JsonResponse({'status': 'Unsuccessful'})

    def Put(self, request):
        data = request.body.decode('utf-8')
        json_data = json.loads(data)
        id = json_data.get('id', None)
        name = json_data.get('name', None)
        birthday = json_data.get('birthday', None)
        owner_name = json_data.get('owner', None)

        owner_obj = Owner.objects.filter(owner__name=owner_name).first()

        cat_object = Cat.objects.filter(id=id).first()
        cat_object.update(owner=owner_obj, name=name, birthday=birthday)

        return JsonResponse({'status': 'successfully updated'})
    def delete(self, request):
        data = request.body.decode('utf-8')
        json_data = json.loads(data)
        
        id = json_data.get('id', None)
        if id:
            Cat.objects.filter(id=id).first().delete()
            return JsonResponse({'status': 'successfully deleted'})
        return  JsonResponse({'status': 'unsuccessfull'})