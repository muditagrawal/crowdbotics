from django.db import models

# Create your models here.
class Owner(models.Model):
    name = models.CharField(max_length=50)

class Dog(models.Model):
    owner = models.ForeignKey('Owner')
    name = models.CharField(max_length=50)
    birthday = models.DateField()

  
class Cat(models.Model):
    owner = models.ForeignKey('Owner')
    name = models.CharField(max_length=50)
    birthday = models.DateField()