from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import DogView, CatView

urlpatterns = [
    url(r'^cat/$', CatView.as_view()),
    url(r'^dog/$', DogView.as_view())
]