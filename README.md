# Crowdbotics Cat and dog pet API



### Setting up
This project was built with python +3.5

```bash
$ virtualenv -p python3 env
$ source ./env/bin/activate
$ pip install -r requirements.txt
$ cd crowdbotics
$ python manage.py runserver
```

Then head to 

http://localhost:8000/api/cat
http://localhost:8000/api/god


in your browser to get started.
